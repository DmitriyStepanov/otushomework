﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;

namespace Lesson_4
{
    [Serializable]
    public class Person: IComparable<Person>
    {
        public string Name { get; set; }
                
        public int Age { get; set; }
        
        private static string[] names = { "Georg", "Liza", "Joe", "Sveta", "Bob", "Igor" };

        private static int[] ages = { 25, 18, 40, 31, 22, 47, 19, 13, 68, 27, 22, 18, 47, 25 };

        public static Person[] CreateRandom(int count)
        {
            var result = new Person[count];

            var randomName = new Random();
            var randomAge = new Random();
            
            for (int i = 0; i < count; i++)
            {
                var name = randomName.Next(Person.names.Length - 1);
                var age = randomAge.Next(Person.ages.Length - 1);

                result[i] = new Person{ 
                
                    Name = names[name],

                    Age = ages[age]
                };
            }

            return result;
        }

        public int CompareTo([AllowNull] Person other)
        {
            if (other == null)
                return -1;

            if (this.Age == other.Age)
                return 0;

            if (this.Age > other.Age)
            {
                return 1;
            }
            else
            {
                return -1;
            }

        }
    }
}
