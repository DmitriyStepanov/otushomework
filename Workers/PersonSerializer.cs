﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using Lesson_4;
using Lesson_4.Interfaces;

namespace Lesson_4.Workers
{
    public class PersonSerializer<T> : IPersonSerializer<Person>
    {
        public void Serialize(string fileName, Person[] personsCollection)
        {
            var sb = new StringBuilder();
            
            for(int i = 0; i < personsCollection.Length; i ++)
            {
                sb.AppendLine($"{personsCollection[i].Name};{personsCollection[i].Age}");
            }

            var bytes = Encoding.Default.GetBytes(sb.ToString());
            
            File.WriteAllBytes(fileName, bytes);
            
        }

        public Person[] Deserialize(String fileName)
        {
            var lines = File.ReadAllLines(fileName);

            var persons = new Person[lines.Length];
            
            for(int i = 0; i < lines.Length; i++)
            {
                var person = lines[i].Split(';');

                persons[i] = new Person
                {
                    Name = person[0],
                    Age = Int32.Parse(person[1])
                };
            }

            return persons;
                
        }
    }
}
