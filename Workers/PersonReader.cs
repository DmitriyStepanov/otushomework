﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using Lesson_4;
using Lesson_4.Interfaces;
using Moq;

namespace Lesson_4.Workers
{
    public class PersonReader<T> : IEnumerable<T>, IDisposable, IPersonSorter<T>, IRepository<T>, IAccountService
    {
        /// <summary>
        /// Имя файла для чтения и записи данных
        /// </summary>
        private readonly string fileName;

        /// <summary>
        /// Объект, реализующий интерфейсы чтения и записи данных в файл / из файла
        /// </summary>
        private readonly IPersonSerializer<T> serializer;

        /// <summary>
        /// Хранит коллекцию данных
        /// </summary>
        private T[] persons;

        /// <summary>
        /// Коллекция для демонстрации работы метода Sort
        /// </summary>
        private List<T> personsCollection;

        /// <summary>
        /// Ctor
        /// </summary>
        /// <param name="fileName"></param>
        /// <param name="serializer"></param>
        public PersonReader(String fileName, IPersonSerializer<T> serializer)
        {
            this.fileName = fileName;

            this.serializer = serializer;

            this.persons = serializer.Deserialize(fileName);
        }

        # region Реализация обобщенного и необобщенного енумератора
        public IEnumerator<T> GetEnumerator()
        {
            for (int i = 0; i < this.persons.Length; i++)
            {
                yield return this.persons[i];
            }
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            for (int i = 0; i < this.persons.Length; i++)
            {
                yield return this.persons[i];
            }
        }
        #endregion

        //Задание номер 2. Реализация какого то интерфейса
        public IEnumerable<T> Sort()
        {
            this.personsCollection = new List<T>();

            foreach(var p in this)
            {
                personsCollection.Add(p);
            }

            this.personsCollection.Sort();

            return this.personsCollection;
        }

        public void Dispose()
        {
            //Просто заглушка, освобождать нечего (потоков, соединение, ничего нет)
        }

        #region //Реализация задания 3
        public IEnumerable<T> GetAll()
        {
            for (int i = 0; i < this.persons.Length; i++)
            {
                yield return this.persons[i];
            }
        }

        public T GetOne(Func<T, bool> predicate)
        {
            return this.Where(predicate).FirstOrDefault();
        }

        public void Add(T item)
        {
            var temp = persons;
            this.persons = new T[persons.Length + 1];

            for (int i = 0; i < persons.Length; i++)
            {
                if (i != persons.Length - 1)
                {
                    persons.SetValue(temp[i], i);
                }
                else
                {
                    persons.SetValue(item, i);
                }

            }
        }
        #endregion

        //Задание номер 3. Без Moq
        public void AddAccount(Person account)
        {

            if (account.Age < 18)
                throw new ArgumentException("Возраст неможет быть менее 18 лет");

            var temp = persons;
            this.persons = new T[persons.Length + 1];

            for (int i = 0; i < persons.Length; i++)
            {
                if (i != persons.Length - 1)
                {
                    persons.SetValue(temp[i], i);
                }
                else
                {
                    persons.SetValue(account, i);
                }
            }

        }

        public void GitTest()
        {
            Console.WriteLine("git test");
        }
        
    }
}
