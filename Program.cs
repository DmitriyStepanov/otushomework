﻿using System;
using System.IO;
using System.Linq;
using Lesson_4.Workers;
using Lesson_4.Interfaces;
using System.Xml;
using System.Xml.Linq;


namespace Lesson_4
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Введите количество объектов Person для инициации данных и нажмите Enter");

            var worker = new Worker();
            int personsCount;
            string count = Console.ReadLine();
            while (!Int32.TryParse(count, out personsCount))
            {
                Console.WriteLine("Введите действительное число");
                count = Console.ReadLine();
            }
            
            worker.Execute(personsCount);
        }
        
    }

    class Worker
    {
        //Имя файла для хранения сериализованных и излвечения данных
        private string fileName = Path.Combine(System.Environment.CurrentDirectory, "persons.csv");

        //Демонстрационные данные
        private Person[] persons;

        IPersonSerializer<Person> seraliser;

        PersonReader<Person> reader;

        #region//переменные для хранения данных нового объекта Person при вооде с коносли
        private string newName = string.Empty;

        private int newAge;

        private string newAgeString = string.Empty;

        #endregion

        public void Execute(int personsCount)
        {
            persons = Person.CreateRandom(personsCount);


            #region Задание 1.Сериализация и десериализация. Хранение данных в файла csv

            Console.WriteLine(new string('*', 15));
            Console.WriteLine("1. Задание: сериализация десирализация");
            Console.WriteLine(new string('*', 15));

            seraliser  = new PersonSerializer<Person>();

            seraliser.Serialize(this.fileName, persons);

            reader = new PersonReader<Person>(fileName, seraliser);

            Console.WriteLine(new String('*', 15));
            Console.WriteLine("Произведена сериализация и десериализация данных");
            Console.WriteLine(new String('*', 15));

            foreach (Person p in reader)
            {
                Console.WriteLine($"Имя: {p.Name}, возраст: {p.Age}");
            }

            var sortedPersons = reader.Sort().ToList();

            #endregion

            #region Задаие 2. Реализация какого интерфейса
            Console.WriteLine(new String('*', 15));
            Console.WriteLine("Задание 2: Реализация в reader IPersonSorter: Отсортированная коллекция persons");
            Console.WriteLine(new String('*', 15));

            foreach (Person p in sortedPersons)
            {
                Console.WriteLine($"Имя: {p.Name}, возраст: {p.Age}");
            }

            #endregion

            #region Задание 3. IRepository
            Console.WriteLine("Нажмите любую клавишу для демонстрации Задания 3");
            Console.ReadKey();
            Console.Clear();

            Console.WriteLine(new String('*', 15));
            Console.WriteLine("Демонстрация коллекции, полученной методом GetAll интефеса IRepository<T>");
            Console.WriteLine(new String('*', 15));

            foreach (Person p in reader.GetAll())
            {
                Console.WriteLine($"Имя: {p.Name}, возраст: {p.Age}");
            }

            Console.WriteLine(new String('*', 15));
            Console.WriteLine("Демонстрация работы метда GetOne(Func<t> bool predicate) Будет отобран самы объект person с самым маленьким значением age");
            Console.WriteLine(new String('*', 15));

            var people = reader.GetAll().ToList();

            var yangest = reader.GetOne(p => p.Age == people.Min(a => a.Age));
            Console.WriteLine($"Имя: {yangest.Name}, возраст: {yangest.Age}");

            Console.WriteLine("Демонстрация работы метда Add(T item)");
            Console.WriteLine("Введите имя");

            this.newName = Console.ReadLine();

            Console.WriteLine("Введите возраст");

            this.newAgeString = Console.ReadLine();

            while (!Int32.TryParse(newAgeString, out newAge))
            {
                Console.WriteLine("Возраст должен быть числом");
                newAgeString = Console.ReadLine();
            }

            reader.Add(new Person
            {
                Name = newName,

                Age = newAge
            });

            Console.WriteLine(new String('*', 15));
            Console.WriteLine("Вывод коллекции после вызова метода Add(T item)");
            Console.WriteLine(new String('*', 15));

            foreach (Person p in reader.GetAll())
            {
                Console.WriteLine($"Имя: {p.Name}, возраст: {p.Age}");
            }

            Console.ReadKey();

            Console.WriteLine("Меттод Add сработал, но вызов на this.Append() из Linq не влиял на коллекцию, пришлось создавать новый массив" +
                "с размерностью + 1, перезаписывать в него старый массив и добавлять вновь созданного person");

            #endregion

            #region Задание 3.1 IAccountService. С Moq неразбирался. Стал делать задаине в только вчера. Неуспел в обще. Исправлюсь.

            Console.WriteLine(new String('*', 15));
            Console.WriteLine("Вызов AddAccount IAccounService ");
            Console.WriteLine(new String('*', 15));

            Console.WriteLine("Введите имя");

            this.newName = Console.ReadLine();

            Console.WriteLine("Введите возраст. Возраст не должен быть меньше 18 лет");

            this.newAgeString = Console.ReadLine();

            while (!Int32.TryParse(newAgeString, out newAge))
            {
                Console.WriteLine("Возраст должен быть числом");
                newAgeString = Console.ReadLine();
            }

        link1:
            try
            {
                reader.AddAccount(new Person
                {
                    Name = newName,

                    Age = newAge
                });
            }
            catch(ArgumentException ex)
            {
                Console.WriteLine(ex.Message);
                Console.WriteLine("Введите возраст. Возраст не должен быть меньше 18 лет");

                this.newAgeString = Console.ReadLine();

                while (!Int32.TryParse(newAgeString, out newAge))
                {
                    Console.WriteLine("Возраст должен быть числом");
                    newAgeString = Console.ReadLine();
                }

                goto link1;
            }
            
            Console.WriteLine(new String('*', 15));
            Console.WriteLine("Вывод коллекции после вызова метода AddAcount(T item)");
            Console.WriteLine(new String('*', 15));

            foreach (Person p in reader.GetAll())
            {
                Console.WriteLine($"Имя: {p.Name}, возраст: {p.Age}");
            }

            Console.ReadKey();

            #endregion
        }
    }
}
