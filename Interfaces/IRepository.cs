﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lesson_4.Interfaces
{
    interface IRepository<T>
    {
        IEnumerable<T> GetAll();
        
        T GetOne(Func<T, bool> predicate);
        
        void Add(T item);
    }
}
