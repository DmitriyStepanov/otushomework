﻿using System;
using System.Collections.Generic;
using System.Text;
using Lesson_4;

namespace Lesson_4.Interfaces
{
    interface IPersonSorter<T>
    {
      IEnumerable<T> Sort();
    }
}
