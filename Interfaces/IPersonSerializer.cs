﻿using System;
using System.Collections.Generic;
using System.Text;
using Lesson_4;
using System.IO;

namespace Lesson_4.Interfaces
{
    public interface IPersonSerializer<T>
    {
        public void Serialize(String data, T[] personsCollection);

        public T[] Deserialize(String fileNmae);
    }
}
